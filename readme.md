# FCU-iVoting
投票網站

## 網址
[http://ivoting.ifcu.today/](http://ivoting.ifcu.today/)

## 基礎架構
- PHP
- MySQL

## 主要框架
- Laravel 5.1
- Bootstrap 3

## 文件格式
- 編碼：UTF8
- 縮排：4 spaces
- 換行：LF
- 去除行尾空格
- 文件結尾有1空白行

（已使用EditorConfig設定完成）

## 推薦IDE
- PhpStorm

（專案使用[EditorConfig](http://editorconfig.org/)定義基本格式，請安裝與IDE對應的套件）